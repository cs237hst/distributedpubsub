package edu.uci.cs237.distributedpubsub.dao;

import java.io.IOException;
import java.util.List;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import edu.uci.cs237.distributedpubsub.model.Message;
import edu.uci.cs237.distributedpubsub.model.PublishMessage;

public class TopicDAOTest {

	private TopicDAO topicDAO;
	private PublishMessage pubMsg;
	
	@Before
	public void setUpTopicDAO(){
		topicDAO = TopicDAO.getInstance();
		pubMsg = new PublishMessage();
		pubMsg.setUserID(1);
		pubMsg.setLocation("Irvine");
		pubMsg.setContent("an example insert message");
		pubMsg.setTopic("testTopic");
	}
	
	@Test
	public void testTopicDAO() throws IOException{
		List<Message> messages = topicDAO.getAllMessagesForTopic("testTopic");
		for(Message m: messages){
			System.out.println(m);
		}
	}
	
	@Test
	public void testCreateTopic(){
		topicDAO.createTopic("newTopic");
	}
	
	@Test
	public void testInsertMessage() throws IOException{
		topicDAO.insertMessage(pubMsg);
		List<Message> messages = topicDAO.getAllMessagesForTopic("testTopic");
		for(Message m: messages){
			System.out.println(m);
		}		
	}
	
	@Test
	public void testGetAllTopics(){
		List<String> topics;
		topicDAO.createTopic("one");
		topics = topicDAO.getAllTopics();
		System.out.println(topics);
		topicDAO.createTopic("two");
		topics = topicDAO.getAllTopics();
		System.out.println(topics);
		topicDAO.createTopic("three");
		topics = topicDAO.getAllTopics();
		System.out.println(topics);
		topicDAO.createTopic("four");
		topics = topicDAO.getAllTopics();
		System.out.println(topics);
	}
	
	@Test
	public void testGetMsgsLaterThan(){
		Long ts = new Long(1);
		List<Message> msgs = topicDAO.getMessagesLaterThan("testTopic", ts);
		System.out.println(msgs);
	}
	
	@Test
	public void testEnrichment(){
		List<String> possibleEnrichments = topicDAO.getRelevantStoresForTopic("earhquake");
		for(String enrichmentStore: possibleEnrichments){
			String extraData = topicDAO.getInfoForLocationFromStore(enrichmentStore, "irvine");
			System.out.println(extraData);
		}
	}
}
