package edu.uci.cs237.distributedpubsub.server;

import java.util.List;

import edu.uci.cs237.distributedpubsub.model.GetAllTopicsMessage;
import edu.uci.cs237.distributedpubsub.model.Message;

/**
 * This class will be tied to the communication layer, which receives
 * incoming messages from clients.
 * @author tanoojp
 *
 */
public class RequestController {

	MessageDispatcher dispatcher;
	
	public RequestController(MessageDispatcher dispatcher)
	{
		this.dispatcher = dispatcher;
	}
	
	public void handleRequest(Message m)
	{
		MessageHandler handler = dispatcher.getHandler(m);
		handler.handleMessage(m);
	}

	public List<String> handleRequestForAllTopics() {
		return ((GetAllTopicsMessageHandler) dispatcher.getHandler(new GetAllTopicsMessage())).handleMessageForAllTopics();
	}

	public List<String> getSubscribedTopicsForUser(Integer userID) {
		return BrokerInitializer.getInstance()
				.subMap.getTopicsForSubscriber(userID);
	}
	
	
}
