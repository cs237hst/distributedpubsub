package edu.uci.cs237.distributedpubsub.model;

import java.util.List;

public class GetAllTopicsMessage implements Message {

	private Integer senderID;
	private List<String> topics;
	
	@Override
	public Integer getUserID() {
		return senderID;
	}

	@Override
	public String getContent() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Class<? extends Message> getType() {
		return this.getClass();
	}

	@Override
	public String getTopic() {
		// TODO Auto-generated method stub
		return null;
	}

	public List<String> getTopics() {
		return topics;
	}

	public void setSenderID(Integer senderID) {
		this.senderID = senderID;
	}

	public void setTopics(List<String> topics) {
		this.topics = topics;
	}
	

}
