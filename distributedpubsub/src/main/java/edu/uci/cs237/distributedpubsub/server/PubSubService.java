package edu.uci.cs237.distributedpubsub.server;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;

import edu.uci.cs237.distributedpubsub.model.CreateTopicMessage;
import edu.uci.cs237.distributedpubsub.model.PublishMessage;
import edu.uci.cs237.distributedpubsub.model.SubscribeMessage;
import edu.uci.cs237.distributedpubsub.model.UnSubscribeMessage;

@Service
public class PubSubService {

    
    public RequestController requestController;
    
    public PubSubService(){
        BrokerInitializer brokerInitializer = BrokerInitializer.getInstance();
        requestController = brokerInitializer.getRequestController();
    }
    
    public void publish(PublishMessage publishMessage) {
        requestController.handleRequest(publishMessage);
        
    }

    public SseEmitter subscribe(String topic, int userID) {
        SubscribeMessage subscribeMessage = new SubscribeMessage();
        subscribeMessage.setUserID(userID);
        subscribeMessage.setTopic(topic);
        requestController.handleRequest(subscribeMessage );
        return PubSubServiceUtils.getSseEmitter(userID);
        //TODO populate SSEEmitter
    }
    
    public void unSubscribe(UnSubscribeMessage unSubscribeMessage) {
        requestController.handleRequest(unSubscribeMessage);
    }
    
    public List<String> getAllTopics(){
/*        List<String> topics = new ArrayList<String>();
        topics.add("earthquake");
        topics.add("tornado");
        topics.add("tsunami");
        topics.add("bomb");
        return topics;*/
    	
    	return requestController.handleRequestForAllTopics();
    }
    
    public void createTopic(CreateTopicMessage createTopicMessage){
        requestController.handleRequest(createTopicMessage);
    }

    public Integer getUserID() {
        return PubSubServiceUtils.generateUserID();
    }

    public List<String> getSubscribedTopics(Integer userID) {
        return requestController.getSubscribedTopicsForUser(userID);
    } 
    
}
