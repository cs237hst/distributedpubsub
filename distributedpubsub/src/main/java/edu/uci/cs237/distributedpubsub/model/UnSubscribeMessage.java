package edu.uci.cs237.distributedpubsub.model;

/**
 * This message is sent by the client to unsubscribe from a topic
 * @author tanoojp
 *
 */
public class UnSubscribeMessage implements Message {

	private Integer userID;
	private String content;
	private String topic;
	
	public Integer getUserID() {
		return userID;
	}

	public String getContent() {
		return content;
	}
	
	public String getTopic(){
		return topic;
	}

	public Class<? extends Message> getType() {
		return UnSubscribeMessage.class;
	}

	public void setUserID(Integer userID) {
		this.userID = userID;
	}

	public void setMessageContent(String content) {
		this.content = content;
	}

	public void setTopic(String topic) {
		this.topic = topic;
	}
	
}
