package edu.uci.cs237.distributedpubsub.server;

import java.util.List;
import java.util.Set;

import edu.uci.cs237.distributedpubsub.communication.MessageSender;
import edu.uci.cs237.distributedpubsub.dao.TopicDAO;
import edu.uci.cs237.distributedpubsub.model.Message;
import edu.uci.cs237.distributedpubsub.model.PublishMessage;

public class PublishMessageHandler implements MessageHandler {

	public void handleMessage(Message m) {
		
		System.out.println("handle publish message");
		String topic = ((PublishMessage) m).getTopic();
		
		//Step 1: Insert message into the database
		if(!topicExists(topic))
		{
			//Topic does not exist, first create a new topic
			TopicDAO.getInstance().createTopic(topic);
		}
		TopicDAO.getInstance().insertMessage(m);
		
		//TODO is the below needed? Polling will send these messages anyway.
		
		//Step 2: Get relevant info from the database, and add to the message
		String relevantInfo = getRelevantInfoForTopic(topic, ((PublishMessage) m).getLocation());
		if(relevantInfo.length()>0)
		{
			((PublishMessage)m).setRelevantInfo(relevantInfo);
		}
		
		//Step 3: Send this message to all interested subscribers at this broker.
		Set<Integer> interestedSubscribers = BrokerInitializer.getInstance().
				subMap.getSubscribersForTopic(topic);
		MessageSender sender = BrokerInitializer.getInstance().getSender();
		for(Integer id: interestedSubscribers)
		{
			sender.sendMessage(id, m);
		}
	}

	private String getRelevantInfoForTopic(String topic, String location) {
		List<String> stores = TopicDAO.getInstance().getRelevantStoresForTopic(topic);
		StringBuffer sb = new StringBuffer();
		if(stores!=null)
		{
			for(String store : stores)
			{
				sb.append(TopicDAO.getInstance().
						getInfoForLocationFromStore(store, location));
				sb.append("\r\n");
			}
		}
		return sb.toString();
	}

	private boolean topicExists(String topic) {
		List<String> allTopics = TopicDAO.getInstance().getAllTopics();
		if(allTopics!=null && allTopics.contains(topic))
			return true;
		return false;
	}

}
