package edu.uci.cs237.distributedpubsub.dao;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import voldemort.client.ClientConfig;
import voldemort.client.SocketStoreClientFactory;
import voldemort.client.StoreClient;
import voldemort.client.StoreClientFactory;
import voldemort.versioning.Versioned;
import edu.uci.cs237.distributedpubsub.model.Message;
import edu.uci.cs237.distributedpubsub.model.PublishMessage;


public class TopicDAO {

	private static TopicDAO topicDAO;
	private TimestampDAO timestampDAO = TimestampDAO.getInstance();
	private EnrichmentDAO enrichmentDAO = EnrichmentDAO.getInstance();
	private PeripheralsDAO peripheralsDAO = PeripheralsDAO.getInstance();
	private StoreClient<Object, Object> storeClient;
	private String bootstrapUrl;
	private ClientConfig clientConfig;
	private StoreClientFactory storeClientFactory;
	private StoreClient<Object, Object> topicListStoreClient;
	
	private TopicDAO(){
		initTopicDAO();
		initTopicListDAO();
	}
	
	private void initTopicListDAO() {
		topicListStoreClient = storeClientFactory.getStoreClient(DAOConstant.TOPIC_LIST_DB_NAME);
//		topicListStoreClient.put("topic", new ArrayList<String>());
	}

	public static TopicDAO getInstance(){
		if(topicDAO == null){
			topicDAO = new TopicDAO();
		}
		return topicDAO;
	}
	/**
	 * to insert via command line put "testTopic2" [{'timestamp':'0', 'message':'testMessage2'}]  
	 * @return
	 */
	
	public List<String> getAllTopics(){
		List<String> topics = new ArrayList<String>();
		Versioned<Object> versioned = topicListStoreClient.get(DAOConstant.TOPIC_LIST_DB_KEY);
		if(versioned == null) return topics;
		Object value = versioned.getValue();
		if(value == null) return topics;
		String t = value.toString();
		List<String> toks = tokenize(t);
		for(String tok: toks){
			String[] keyVal = tok.split("=");
			keyVal[1] = keyVal[1].trim();
			topics.add(keyVal[1]);
		}
		return topics;
	}
	public List<Message> getMessagesForTopic(String topic){
		return null;}
	
	
	private void initTopicDAO(){//server has to be started beforehand
		bootstrapUrl = DAOConstant.getBootstrapUrl();
		clientConfig = new ClientConfig().setBootstrapUrls(bootstrapUrl);
		storeClientFactory = new SocketStoreClientFactory(clientConfig);
		storeClient = storeClientFactory.getStoreClient(DAOConstant.TOPIC_CLIENT_NAME);
	}
	/**
	 * @return List<Message>
	 * @param topic
	 * @throws IOException
	 */
	public List<Message> getAllMessagesForTopic(String topic) throws IOException{
		List<Message> messages = new ArrayList<Message>();
		String jsonMessages = storeClient.get(topic).getValue().toString();
		List<String> msgs = tokenize(jsonMessages);
		for(String msg: msgs){
			String[] msgComponents = msg.split(",");
			PublishMessage pubMsg = new PublishMessage();
			pubMsg.setTopic(topic);
			for(String msgComponent: msgComponents){
				String[] keyVal = msgComponent.split("=");
				if(keyVal.length != 2) System.err.println("Parse error");
				keyVal[0] = keyVal[0].trim();
				keyVal[1] = keyVal[1].trim();
				if(keyVal[0].equals("message")){
					pubMsg.setContent(keyVal[1]);
				} else if(keyVal[0].equals("location")){
					pubMsg.setLocation(keyVal[1]);
				} else if(keyVal[0].equals("senderID")){
					pubMsg.setUserID(Integer.parseInt(keyVal[1]));
				} else if(keyVal[0].equals("timestamp")){}
			}
			messages.add(pubMsg);
		}
		System.out.println("Returned " + messages.size() + " number of messages");
		return messages;
	}
	/**
	 * returns the list of message string without flower braces
	 * @param jsonMessages
	 * @return
	 */
	public static List<String> tokenize(String jsonMessages) {
		List<String> msgs = new ArrayList<String>();
		int index = 0, limit = jsonMessages.length();
		while(index < limit){
			while(index < limit && jsonMessages.charAt(index) != '{') index++;
			index++;
			String m = "";
			while(index < limit && jsonMessages.charAt(index) != '}'){
				m = m + jsonMessages.charAt(index);
				index++;
			}
			if(!m.isEmpty()) msgs.add(m);
		}
		return msgs;
	}

	/**
	 * returns the messages that were published later than timestamp for a particular topic
	 * @param topic
	 * @param timestamp
	 * @return
	 */
	public List<Message> getMessagesLaterThan(String topic, Long timestamp){
		List<Message> msgs = new ArrayList<Message>();
		String allMsgs = storeClient.get(topic).getValue().toString();
		List<String> toks = tokenize(allMsgs);
		boolean qualified = false;
		for(String msg: toks){
			String[] components = msg.split(",");
			String message = "", location = "", time = "";
			int userID = -1;
			for(String component: components){
				String[] keyVal = component.split("=");
				keyVal[0] = keyVal[0].trim();
				keyVal[1] = keyVal[1].trim();
				if(keyVal[0].equals("message")){
					message = keyVal[1];
				} else if(keyVal[0].equals("timestamp")){
					Long ts = Long.valueOf(keyVal[1]);
					if(ts >= timestamp){
						qualified = true;
					}
					time = keyVal[1];
				} else if(keyVal[0].equals("location")){
					location = keyVal[1];
				} else if(keyVal[0].equals("senderID")){
					userID = Integer.parseInt(keyVal[1]);
				}
			}
			if(qualified){
				PublishMessage pubMsg = new PublishMessage();
				pubMsg.setLocation(location);
				pubMsg.setContent(message);
				pubMsg.setTopic(topic);
				pubMsg.setUserID(userID);
				msgs.add(pubMsg);
			}
		}
		return msgs;
	}
	public void createTopic(String topic){
		Versioned<Object> msg = storeClient.get(topic);
		List<String> msgs = new ArrayList<String>();
		storeClient.put(topic, msgs);
		timestampDAO.createNewTimestampForTopic(topic);
		addToTopicList(topic);
	}
	
	private void addToTopicList(String topic) {
		Versioned<Object> versioned = topicListStoreClient.get(DAOConstant.TOPIC_LIST_DB_KEY);
		if(versioned == null){
			topicListStoreClient.put(DAOConstant.TOPIC_LIST_DB_KEY, new ArrayList<String>());
			versioned = topicListStoreClient.get(DAOConstant.TOPIC_LIST_DB_KEY);
		}
		String topics = versioned.getValue().toString();
		List<String> tokenizedTopics = tokenize(topics);
		List<Map> topicsList = new ArrayList<Map>();
		for(String s: tokenizedTopics){
			Map<String, String> existingTopic = new HashMap<String, String>();
			String[] toks = s.split("=");
			toks[0] = toks[0].trim();
			toks[1] = toks[1].trim();
			existingTopic.put(toks[0], toks[1]);
			topicsList.add(existingTopic);
		}
		Map<String, String> newTopic = new HashMap<String, String>();
		newTopic.put(DAOConstant.TOPIC_LIST_DB_KEY, topic);
		topicsList.add(newTopic);
		topicListStoreClient.put(DAOConstant.TOPIC_LIST_DB_KEY, topicsList);
	}

	public void createTopicIfNotPresent(String topic){
		Versioned<Object> msg = storeClient.get(topic);
		if(msg == null){
			List<String> msgs = new ArrayList<String>();
			storeClient.put(topic, msgs);
		}
	}
	
	private String getFormattedMsg(String msg, String ts, String location, String id){
		return "message=" + msg + ", timestamp=" + ts + 
				", location=" + location + ", senderID=" + id;
	}
	
	/**
	 * Reads the topic in the message, and inserts the message accordingly.
	 * This method assumes that the topic on the message already exists.
	 * Ideally, should be called only when a message gets published
	 * @param m
	 */
	public void insertMessage(Message m){
		Class<? extends Message> messageClass = m.getType();
		String topic = m.getTopic();
		createTopicIfNotPresent(topic);
		if(messageClass == PublishMessage.class){
			PublishMessage pubMsg = (PublishMessage) m;
			Long ts = timestampDAO.getTimestampForNewMessage(topic);
			String timestamp = ts.toString();
			String msg = pubMsg.getContent();
			String senderID = pubMsg.getUserID().toString();
			String location = pubMsg.getLocation();
			String messageFormat = getFormattedMsg(msg, timestamp, location, senderID);
			String existingMsgs = storeClient.get(topic).getValue().toString();
			List<String> msgs = tokenize(existingMsgs);
			msgs.add(messageFormat);
			List<Map> toAdd = new ArrayList<Map>();
			for(String str: msgs){
				Map<String, String> map = getMapFromMessage(str);
				toAdd.add(map);
			}
			storeClient.put(topic, toAdd);
		}
	}

	private Map<String, String> getMapFromMessage(String message) {
		Map<String, String> map = new TreeMap<String, String>();
		String[] components = message.split(",");
		for(String comp: components){
			String[] keyVal = comp.split("=");
			keyVal[0] = keyVal[0].trim();
			keyVal[1] = keyVal[1].trim();
			if(keyVal.length != 2) System.err.println("Parse error");
			map.put(keyVal[0], keyVal[1]);
		}
		return map;
	}

	public List<String> getRelevantStoresForTopic(String topic) {
		return enrichmentDAO.getEnrichmentStoresForTopic(topic);
	}

	public String getInfoForLocationFromStore(String store, String location) {
		String legend = null;
		if(store.equals(DAOConstant.CONTACT_STORE_NAME)){
			legend = "Helplines";
			peripheralsDAO.setContactStore();
		}
		else if(store.equals(DAOConstant.SHELTER_STORE_NAME)){
			legend = "Shelters";
			peripheralsDAO.setShelterStore();
		}
		List<String> ret = peripheralsDAO.getPeripheralData(location);
		StringBuilder strBuilder = new StringBuilder();
		strBuilder.append(legend).append(": [");
		for(String str: ret){
			strBuilder.append(str).append(", ");
		}
		strBuilder.append("]");
		return strBuilder.toString();
	}

//	public void addElement(String string, String string2) {
////		client.put(string, string2);
//		System.out.println("Inserted");
//	}
	
}
