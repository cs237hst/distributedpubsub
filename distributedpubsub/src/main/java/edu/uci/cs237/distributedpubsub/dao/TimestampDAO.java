package edu.uci.cs237.distributedpubsub.dao;

import voldemort.client.ClientConfig;
import voldemort.client.SocketStoreClientFactory;
import voldemort.client.StoreClient;
import voldemort.client.StoreClientFactory;
import voldemort.versioning.Versioned;

public class TimestampDAO {
	
	private static TimestampDAO timestampDAO;
	StoreClient<String, String> storeClient;
	private String bootstrapUrl;
	private ClientConfig clientConfig;
	private StoreClientFactory storeClientFactory;
	
	
	private TimestampDAO(){
		initTimestampDAO();
	}
	
	private void initTimestampDAO() {
		bootstrapUrl = DAOConstant.getBootstrapUrl();
		clientConfig = new ClientConfig().setBootstrapUrls(bootstrapUrl);
		storeClientFactory = new SocketStoreClientFactory(clientConfig);
		storeClient = storeClientFactory.getStoreClient(DAOConstant.TIMESTAMP_CLIENT_NAME);
	}

	public static TimestampDAO getInstance(){
		if(timestampDAO == null){
			timestampDAO = new TimestampDAO();
		}
		return timestampDAO;
	}
	
	public Long getMostRecentTimestampForTopic(String topic) {
		Versioned<String> versioned = storeClient.get(topic);
		if(versioned == null){//new topic, create a timestamp
			createNewTimestampForTopic(topic);
		}
		String timestamp = String.valueOf(versioned.getValue());
		return Long.valueOf(timestamp).longValue();
	}

	public void updateTimestampForTopic(String topic){
		Versioned<String> versioned = storeClient.get(topic);
		String timestamp = String.valueOf(versioned.getValue());
		Long newTimestamp = Long.valueOf(timestamp).longValue() + 1;
		String updatedTs = newTimestamp.toString();
		versioned.setObject(updatedTs);
		storeClient.put(topic, updatedTs);
	}
	public void createNewTimestampForTopic(String topic) {
		storeClient.put(topic, "0");
	}

	public Long getTimestampForNewMessage(String topic) {
		
		updateTimestampForTopic(topic);
		return getMostRecentTimestampForTopic(topic);
	}
}
