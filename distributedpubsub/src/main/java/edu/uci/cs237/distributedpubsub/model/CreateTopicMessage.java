package edu.uci.cs237.distributedpubsub.model;

public class CreateTopicMessage implements Message {
	
	private Integer userID;
	private String topic;

	
	@Override
	public Integer getUserID() {
		return userID;
	}

	@Override
	public String getContent() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Class<? extends Message> getType() {
		return this.getClass();
	}

	@Override
	public String getTopic() {
		return topic;
	}

	public void setUserID(Integer userID) {
		this.userID = userID;
	}

	public void setTopic(String topic) {
		this.topic = topic;
	}

}
