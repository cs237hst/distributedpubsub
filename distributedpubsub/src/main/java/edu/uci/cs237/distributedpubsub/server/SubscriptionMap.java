package edu.uci.cs237.distributedpubsub.server;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;

/**
 * This class maintains a mapping of all subscribers at a broker interested 
 * in any topic, and the timestamp of the latest message read by the broker
 * for each topic.
 * @author tanoojp
 *
 */
public class SubscriptionMap {

	private HashMap<String, Set<Integer>> topicSubscribersMap = new HashMap<String, Set<Integer>>();
	private HashMap<String, Long> topicTimestampMap = new HashMap<String, Long>();
	
	/**
	 * Broker is interested in the union of all topics that subscribers are 
	 * interested in
	 * @return
	 */
	public Set<String> getInterestedTopics() {
		return topicSubscribersMap.keySet();
	}
	
	public Set<Integer> getSubscribersForTopic(String topic)
	{
		return topicSubscribersMap.get(topic);
	}
	
	public void addSubscriber(String topic, Integer id)
	{
		if(topicSubscribersMap.containsKey(topic))
		{
			topicSubscribersMap.get(topic).add(id);
		}
		else
		{
			HashSet<Integer> idSet = new HashSet<Integer>();
			idSet.add(id);
			topicSubscribersMap.put(topic, idSet);
		}
	}

	public void removeSubscriber(String topic, Integer senderID) {
		if(!topicSubscribersMap.containsKey(topic))
		{
			//No subscribers exist for this topic anyway, ignore
			return;
		}
		Set<Integer> subscribers = topicSubscribersMap.get(topic);
		subscribers.remove(senderID);
		if(subscribers.size()==0)
		{
			//We don't have any subscribers interested in this topic anymore
			//We (i.e. the broker) can stop being interested in this topic
			topicSubscribersMap.remove(topic);
		}
	}
	
	public synchronized void updateTimeStamp(String topic, Long timestamp)
	{
		topicTimestampMap.put(topic, timestamp);
	}
	
	public synchronized Long getLastUpdatedTimeStamp(String topic)
	{
		Long timestamp = topicTimestampMap.get(topic);
		if(timestamp==null)
		{
			timestamp = new Long(0);
		}
		return timestamp;
	}
	
	public List<String> getTopicsForSubscriber(Integer userID)
	{
		List<String> topics = new ArrayList<>();
		for(Entry<String, Set<Integer>> entry: topicSubscribersMap.entrySet())
		{
			if(entry.getValue()!=null && entry.getValue().contains(userID))
			{
				topics.add(entry.getKey());
			}
		}
		return topics;
	}
}
