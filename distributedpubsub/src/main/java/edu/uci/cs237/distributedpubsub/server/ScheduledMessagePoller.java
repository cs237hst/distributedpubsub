package edu.uci.cs237.distributedpubsub.server;

import java.util.List;
import java.util.Set;

import edu.uci.cs237.distributedpubsub.communication.MessageSender;
import edu.uci.cs237.distributedpubsub.dao.TimestampDAO;
import edu.uci.cs237.distributedpubsub.dao.TopicDAO;
import edu.uci.cs237.distributedpubsub.model.Message;
import edu.uci.cs237.distributedpubsub.model.PublishMessage;

/**
 * Wakes up after every REFRESH_INTERVAL and polls the database for new messages
 * on any of the topics that the broker is interested. If new messages are found
 * they are sent to all interested subscribers
 * @author tanoojp
 *
 */
public class ScheduledMessagePoller implements Runnable{

	public static final int REFRESH_INTERVAL = 1000;
	private MessageSender sender;
	private SubscriptionMap map;

	
	
	public ScheduledMessagePoller(MessageSender sender, SubscriptionMap map) {
		this.sender = sender;
		this.map = map;
	}

	@Override
    public void run() {
		while(true)
		{
			try {
				Thread.sleep(REFRESH_INTERVAL);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			poll();
		}
	}

	private void poll() {
//		System.out.println("Poll");
		for (String topic : map.getInterestedTopics()) {
			Long lastUpdatedTimeStampForBroker = map.getLastUpdatedTimeStamp(topic);
			Long mostRecentTimestampFromDB = TimestampDAO.getInstance().getMostRecentTimestampForTopic(topic);
			if (mostRecentTimestampFromDB > lastUpdatedTimeStampForBroker) {
				// New messages are present in the database, send them to subscribers
				List<Message> newMessages = TopicDAO.getInstance().getMessagesLaterThan(topic,
						lastUpdatedTimeStampForBroker);
				Set<Integer> subscribersForTopic = map.getSubscribersForTopic(topic);
				for (Integer id : subscribersForTopic) {
					for(Message m: newMessages)
					{
						String relevantInfo = getRelevantInfoForTopic(topic, ((PublishMessage) m).getLocation());
						if(relevantInfo.length()>0)
						{
							((PublishMessage)m).setRelevantInfo(relevantInfo);
						}
						
						System.out.println("Sending message: " + m.getContent());
						System.out.println("To ID: " + id);
						sender.sendMessage(id, m);
					}

				}
				//Update the lastUpdatedTimeStamp
				map.updateTimeStamp(topic, mostRecentTimestampFromDB);
			}
		}
		
		
	}
	
	private String getRelevantInfoForTopic(String topic, String location) {
		List<String> stores = TopicDAO.getInstance().getRelevantStoresForTopic(topic);
		StringBuffer sb = new StringBuffer();
		if(stores!=null)
		{
			for(String store : stores)
			{
				sb.append(TopicDAO.getInstance().
						getInfoForLocationFromStore(store, location));
				sb.append("\r\n");
			}
		}
		return sb.toString();
	}
}
