package edu.uci.cs237.distributedpubsub.model;

/**
 * This message is sent as a publish event on a topic
 * @author tanoojp
 *
 */
public class PublishMessage implements Message {

	private Integer userID;
	private String content;
	private String topic;
	private String location;
	private String relevantInfo;
	
	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	@Override
    public Integer getUserID() {
		return userID;
	}

	@Override
    public String getContent() {
		return content;
	}
	
	@Override
    public String getTopic(){
		return topic;
	}

	public void setUserID(Integer userID) {
		this.userID = userID;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public void setTopic(String topic) {
		this.topic = topic;
	}

	@Override
    public Class<? extends Message> getType() {
		return PublishMessage.class;
	}
	
	@Override
    public String toString(){
		return this.topic + ": " + this.content;
	}

	public String getRelevantInfo() {
		return relevantInfo;
	}

	public void setRelevantInfo(String relevantInfo) {
		this.relevantInfo = relevantInfo;
	}

}
