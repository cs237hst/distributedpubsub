package edu.uci.cs237.distributedpubsub.communication;

import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;

import edu.uci.cs237.distributedpubsub.model.Message;
import edu.uci.cs237.distributedpubsub.server.PubSubServiceUtils;

/**
 * Sends messages to the clients based on the ID. Maintains a map of subscriber
 * ID to the actual network identity of the client.
 * 
 * @author tanoojp
 *
 */
public class MessageSender {

	public void sendMessage(Integer id, Message m) {
		SseEmitter sseEmitter = PubSubServiceUtils.getSseEmitter(id);
		if (sseEmitter == null) {
			System.err.println("Could not find SSE Emitter for userID: " + id);
		} else {
			try {
				sseEmitter.send(m);
			} catch (Exception e) {
				System.err.println("Could not send message to userID: " + id);
				e.printStackTrace();
			}
		}
	}
};
