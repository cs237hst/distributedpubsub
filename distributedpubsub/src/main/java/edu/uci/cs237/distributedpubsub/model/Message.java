package edu.uci.cs237.distributedpubsub.model;

public interface Message {

	public Integer getUserID();
	public String getContent();
	public Class<? extends Message> getType();
	public String getTopic();
	
}
