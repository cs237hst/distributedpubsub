package edu.uci.cs237.distributedpubsub.server;

import edu.uci.cs237.distributedpubsub.model.Message;

public interface MessageHandler {

	public void handleMessage(Message m);
}
