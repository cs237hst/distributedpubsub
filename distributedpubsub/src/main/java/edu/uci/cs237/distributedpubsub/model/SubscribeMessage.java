package edu.uci.cs237.distributedpubsub.model;

/**
 * This message is sent by the client to subscribe to a topic
 * @author tanoojp
 *
 */
public class SubscribeMessage implements Message {

	private Integer userID;
	private String topic;
	
	@Override
    public Integer getUserID() {
		return userID;
	}
	
	@Override
    public String getTopic(){
		return topic;
	}

	@Override
    public Class<? extends Message> getType() {
		return SubscribeMessage.class;
	}
	
	public void setUserID(Integer userID) {
		this.userID = userID;
	}

	public void setTopic(String topic) {
		this.topic = topic;
	}

    @Override
    public String getContent() {
        return null;
    }

}
