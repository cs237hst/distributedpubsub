package edu.uci.cs237.distributedpubsub.server;

import java.util.List;

import edu.uci.cs237.distributedpubsub.dao.TopicDAO;
import edu.uci.cs237.distributedpubsub.model.Message;

public class CreateTopicMessageHandler implements MessageHandler {

	@Override
	public void handleMessage(Message m) {
		String topic = m.getTopic();
		List<String> allTopics = TopicDAO.getInstance().getAllTopics();
		if(allTopics!=null && allTopics.contains(topic))
		{
			//Topic already exists
			return;
		}
		TopicDAO.getInstance().createTopic(topic);
	}

}
