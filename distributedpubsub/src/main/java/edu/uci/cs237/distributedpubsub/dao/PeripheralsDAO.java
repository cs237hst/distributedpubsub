package edu.uci.cs237.distributedpubsub.dao;

import java.util.ArrayList;
import java.util.List;

import voldemort.client.ClientConfig;
import voldemort.client.SocketStoreClientFactory;
import voldemort.client.StoreClient;
import voldemort.client.StoreClientFactory;
import voldemort.versioning.Versioned;

public class PeripheralsDAO {
	private static PeripheralsDAO peripheralsDAO;
	private StoreClient<Object, Object> storeClient;
	private String bootstrapUrl;
	private ClientConfig clientConfig;
	private StoreClientFactory storeClientFactory;

	
	private PeripheralsDAO(){
		bootstrapUrl = DAOConstant.getBootstrapUrl();
		clientConfig = new ClientConfig().setBootstrapUrls(bootstrapUrl);
		storeClientFactory = new SocketStoreClientFactory(clientConfig);
	}
	public static PeripheralsDAO getInstance(){
		if(peripheralsDAO == null){
			peripheralsDAO = new PeripheralsDAO();
		}
		return peripheralsDAO;
	}
	
	public void setShelterStore(){
		storeClient = storeClientFactory.getStoreClient(DAOConstant.SHELTER_STORE_NAME);
	}
	
	public void setContactStore(){
		storeClient = storeClientFactory.getStoreClient(DAOConstant.CONTACT_STORE_NAME);
	}
	/**
	 * Assumption: All peripheral stores follow the schema of location vs list of {something}
	 */
	public List<String> getPeripheralData(String key){
		List<String> peripheralDataList = new ArrayList<String>();
		Versioned<Object> versioned = storeClient.get(key);
		if(versioned == null) return peripheralDataList;
		Object value = versioned.getValue();
		if(value == null) return peripheralDataList;
		String peripheralData = value.toString();
		List<String> toks = TopicDAO.tokenize(peripheralData);
		for(String tok: toks){
			String[] keyVal = tok.split("=");
			peripheralDataList.add(keyVal[1].trim());
		}
		return peripheralDataList;
	}
}
