package edu.uci.cs237.distributedpubsub.server;

import edu.uci.cs237.distributedpubsub.communication.MessageSender;

/**
 * This class initializes all components of the broker, and holds
 * references to any objects that could be required by different components.
 * @author tanoojp
 *
 */
public class BrokerInitializer {

	private static BrokerInitializer instance;
	RequestController requestController;
	MessageDispatcher dispatcher;
	SubscriptionMap subMap;
	MessageSender sender;
	ScheduledMessagePoller poller;
	
	private BrokerInitializer()
	{
		init();
	}

	private void init() {
		dispatcher = new MessageDispatcher();
		requestController = new RequestController(dispatcher);
		subMap = new SubscriptionMap();
		sender = new MessageSender();
		poller = new ScheduledMessagePoller(sender, subMap);
		Thread t = new Thread(poller);
		t.start();
	}
	
	public RequestController getRequestController() {
		return requestController;
	}

	public static BrokerInitializer getInstance()
	{
		if(instance==null)
		{
		    synchronized (BrokerInitializer.class) {
                if(instance == null){
                    instance = new BrokerInitializer();
                }
            }
			
		}
		return instance;
	}

	public SubscriptionMap getSubMap() {
		return subMap;
	}

	public MessageSender getSender() {
		return sender;
	}
	
}
