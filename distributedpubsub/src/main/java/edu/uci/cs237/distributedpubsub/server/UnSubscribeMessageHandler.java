package edu.uci.cs237.distributedpubsub.server;

import edu.uci.cs237.distributedpubsub.model.Message;
import edu.uci.cs237.distributedpubsub.model.UnSubscribeMessage;

public class UnSubscribeMessageHandler implements MessageHandler {

	public void handleMessage(Message m) {
		
		String topic = ((UnSubscribeMessage) m).getTopic();
		Integer senderID = m.getUserID();
		
		//Step 1: Remove subscriber from subscription map for the broker
		BrokerInitializer.getInstance().subMap.removeSubscriber(topic, senderID);
			
	}

}
