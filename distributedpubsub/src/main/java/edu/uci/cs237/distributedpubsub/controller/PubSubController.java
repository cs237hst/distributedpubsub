package edu.uci.cs237.distributedpubsub.controller;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;

import edu.uci.cs237.distributedpubsub.model.CreateTopicMessage;
import edu.uci.cs237.distributedpubsub.model.PublishMessage;
import edu.uci.cs237.distributedpubsub.model.UnSubscribeMessage;
import edu.uci.cs237.distributedpubsub.server.PubSubService;

@Controller
@RequestMapping("/pubsub")
public class PubSubController {
    @Autowired
    private PubSubService pubSubService;
    
    @RequestMapping(value="/userid", method=RequestMethod.GET)
    public @ResponseBody Integer getUserID(){
        return pubSubService.getUserID();
    }
    
    @RequestMapping(value="/publish", method = RequestMethod.POST)
    @ResponseStatus(value = HttpStatus.OK)
    public void publish(@RequestBody PublishMessage publishMessage) throws IOException{
        pubSubService.publish(publishMessage);
    }
    
    @RequestMapping(value="/subscribe", method = RequestMethod.GET)
    public SseEmitter subscribe(@RequestParam(value="topic", required=true)String topic, 
            @RequestParam(value="userid", required=true) int userID){
        return pubSubService.subscribe(topic, userID);
    }
    
    @RequestMapping(value="/unsubscribe", method = RequestMethod.POST)
    @ResponseStatus(value = HttpStatus.OK)
    public void unSubscribe(@RequestBody UnSubscribeMessage unSubscribeMessage){
        pubSubService.unSubscribe(unSubscribeMessage);
    }
    
    @RequestMapping(value="/topic", method = RequestMethod.GET)
    public @ResponseBody List<String> getAllTopics(){
        return pubSubService.getAllTopics();
    }
    
    @RequestMapping(value="/topic", method = RequestMethod.POST)
    @ResponseStatus(value = HttpStatus.OK)
    public void createTopic(@RequestBody CreateTopicMessage createTopicMessage){
        pubSubService.createTopic(createTopicMessage);
    }
    
    @RequestMapping(value="/user/{userID}/topic", method=RequestMethod.GET)
    public List<String> getSubsrcribedTopics(@PathVariable(value="userID") Integer userID){
        return pubSubService.getSubscribedTopics(userID);
    }
    
    
}