package edu.uci.cs237.distributedpubsub.server;

import edu.uci.cs237.distributedpubsub.model.CreateTopicMessage;
import edu.uci.cs237.distributedpubsub.model.Message;
import edu.uci.cs237.distributedpubsub.model.PublishMessage;
import edu.uci.cs237.distributedpubsub.model.SubscribeMessage;
import edu.uci.cs237.distributedpubsub.model.UnSubscribeMessage;

public abstract class TestServer {

	public static void main(String[] args) {
		BrokerInitializer bi = BrokerInitializer.getInstance();
		CreateTopicMessage createMessage = new CreateTopicMessage();
		createMessage.setTopic("earthquake");
		bi.getRequestController().handleRequest(createMessage );
		subscribe(bi);
		
		publish(bi);	
		unsubscribe(bi);
		
	}

	private static void unsubscribe(BrokerInitializer bi) {
		UnSubscribeMessage m = new UnSubscribeMessage();
		m.setTopic("earthquake");
		m.setUserID(2);
		bi.getRequestController().handleRequest(m);
		System.out.println(bi.getSubMap().getSubscribersForTopic("earthquake"));
		
	}

	private static void subscribe(BrokerInitializer bi) {
		SubscribeMessage m = new SubscribeMessage();
		m.setTopic("earthquake");
		m.setUserID(2);
		bi.getRequestController().handleRequest(m);
		System.out.println(bi.getSubMap().getSubscribersForTopic("earthquake"));
	}

	private static void publish(BrokerInitializer bi) {
		PublishMessage m = new PublishMessage();
		m.setUserID(1);
		m.setTopic("google");
		m.setContent("First message");
		bi.getRequestController().handleRequest(m);
	}

}
