package edu.uci.cs237.distributedpubsub.server;

import java.util.List;

import edu.uci.cs237.distributedpubsub.dao.TopicDAO;
import edu.uci.cs237.distributedpubsub.model.GetAllTopicsMessage;
import edu.uci.cs237.distributedpubsub.model.Message;

public class GetAllTopicsMessageHandler implements MessageHandler {

	@Override
	public void handleMessage(Message m) {
		// TODO Auto-generated method stub
	}

	public List<String> handleMessageForAllTopics()
	{
		return TopicDAO.getInstance().getAllTopics();
	}
}
