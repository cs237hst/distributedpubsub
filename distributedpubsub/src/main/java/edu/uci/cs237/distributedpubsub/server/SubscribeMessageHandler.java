package edu.uci.cs237.distributedpubsub.server;

import edu.uci.cs237.distributedpubsub.model.Message;
import edu.uci.cs237.distributedpubsub.model.SubscribeMessage;

public class SubscribeMessageHandler implements MessageHandler {

	public void handleMessage(Message m) {
		
		String topic = ((SubscribeMessage) m).getTopic();
		Integer senderID = m.getUserID();
		
		//Step 1: Add subscriber to subscription map for the broker
		BrokerInitializer.getInstance().subMap.addSubscriber(topic, senderID);
			
	}

}
