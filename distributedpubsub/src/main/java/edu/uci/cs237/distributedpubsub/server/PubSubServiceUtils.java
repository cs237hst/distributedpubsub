package edu.uci.cs237.distributedpubsub.server;

import java.util.HashMap;
import java.util.Map;

import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;

public class PubSubServiceUtils {
    
    private static Map<Integer, SseEmitter> userIdVsEmitter = new HashMap<Integer, SseEmitter>();
    private static Integer userID = 0;
    
    public static SseEmitter getSseEmitter(int userID){
        SseEmitter sseEmitter = userIdVsEmitter.get(userID);
        if(sseEmitter == null){
            synchronized (PubSubServiceUtils.userIdVsEmitter) {
                if(sseEmitter == null){
                    sseEmitter = new SseEmitter(Long.MAX_VALUE);
                    userIdVsEmitter.put(userID, sseEmitter);
                }
            }
        }
        return sseEmitter;
    }

    public static Integer generateUserID() {
        synchronized (PubSubServiceUtils.userID) {
            userID = userID + 1;
        }
        return userID;
    }
    

}
