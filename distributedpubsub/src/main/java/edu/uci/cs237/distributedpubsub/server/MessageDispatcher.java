package edu.uci.cs237.distributedpubsub.server;

import java.util.HashMap;

import edu.uci.cs237.distributedpubsub.model.CreateTopicMessage;
import edu.uci.cs237.distributedpubsub.model.GetAllTopicsMessage;
import edu.uci.cs237.distributedpubsub.model.Message;
import edu.uci.cs237.distributedpubsub.model.PublishMessage;
import edu.uci.cs237.distributedpubsub.model.SubscribeMessage;
import edu.uci.cs237.distributedpubsub.model.UnSubscribeMessage;

/**
 * Provides an instance of the correct message handler according to the 
 * type of message.
 * @author tanoojp
 *
 */
public class MessageDispatcher {

	HashMap<Class<? extends Message>, MessageHandler> handlerMap = new HashMap<Class<? extends Message>, MessageHandler>();
	
	public MessageDispatcher()
	{
		handlerMap.put(SubscribeMessage.class, new SubscribeMessageHandler());
		handlerMap.put(UnSubscribeMessage.class, new UnSubscribeMessageHandler());
		handlerMap.put(PublishMessage.class, new PublishMessageHandler());
		handlerMap.put(CreateTopicMessage.class, new CreateTopicMessageHandler());
		handlerMap.put(GetAllTopicsMessage.class, new GetAllTopicsMessageHandler());
	}
	
	public MessageHandler getHandler(Message m)
	{
		return handlerMap.get(m.getType());
	}
}
