package edu.uci.cs237.distributedpubsub.dao;

import java.util.ArrayList;
import java.util.List;

import voldemort.client.ClientConfig;
import voldemort.client.SocketStoreClientFactory;
import voldemort.client.StoreClient;
import voldemort.client.StoreClientFactory;
import voldemort.versioning.Versioned;

public class EnrichmentDAO {
	private static EnrichmentDAO enrichmentDAO;
	StoreClient<Object, Object> storeClient;
	private String bootstrapUrl;
	private ClientConfig clientConfig;
	private StoreClientFactory storeClientFactory;
	private EnrichmentDAO(){
		bootstrapUrl = DAOConstant.getBootstrapUrl();
		clientConfig = new ClientConfig().setBootstrapUrls(bootstrapUrl);
		storeClientFactory = new SocketStoreClientFactory(clientConfig);
		storeClient = storeClientFactory.getStoreClient(DAOConstant.ENRICHMENT_STORE_NAME);
	}
	public static EnrichmentDAO getInstance(){
		if(enrichmentDAO == null){
			enrichmentDAO = new EnrichmentDAO();
		}
		return enrichmentDAO;
	}
	public List<String> getEnrichmentStoresForTopic(String topic){
		List<String> enrichments = new ArrayList<String>();
		Versioned<Object> versioned = storeClient.get(topic);
		if(versioned == null) return enrichments;
		Object value = versioned.getValue();
		if(value == null) return enrichments;
		String stores = value.toString();
		List<String> storesToks = TopicDAO.tokenize(stores);
		for(String str: storesToks){
			String[] keyVal = str.split("=");
			enrichments.add(keyVal[1].trim());
		}
		return enrichments;
	}
}
