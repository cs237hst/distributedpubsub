package edu.uci.cs237.distributedpubsub.dao;

public class DAOConstant {
	
	public static final String PROTOCOL = "tcp";
	public static final String VOLDEMORT_DB_HOST = "localhost";
	public static final String VOLDEMORT_DB_PORT = "10523";
	public static final String TIMESTAMP_CLIENT_NAME = "timestampStore";
	public static final String TOPIC_CLIENT_NAME = "topicStore3";
	public static final String TOPIC_LIST_DB_NAME = "topicList";
	public static final String TOPIC_LIST_DB_KEY = "topic";
	public static final String ENRICHMENT_STORE_NAME = "enrichmentStore2";
	public static final String SHELTER_STORE_NAME = "shelterStore2";
	public static final String CONTACT_STORE_NAME = "contactStore2";
	//rest of the sites here - TODO
	
	public static final String getBootstrapUrl(){
		return PROTOCOL + "://" + VOLDEMORT_DB_HOST + ":" + VOLDEMORT_DB_PORT;
	}
}
