angular.module('myApp', []).controller('myController', function($scope, $http){
	$scope.topics = [];
	$scope.messages = [];
//	$scope.currentMessage = {"content":"sample content", "topic":"earthquake", "location":"Irvine", "relevantInfo":"sample relevant info"};
	
	$scope.subscribedTopics = [];
	
	$scope.publish = function(){
		$scope.publishSuccessMessage = false;
		$scope.publishFailureMessage = false;
		$scope.publishMessage.userID = $scope.userID;
		console.log("publishing message to the server");
		console.log($scope.publishMessage);
		
		$http.post('/cs2372/rest/pubsub/publish', $scope.publishMessage).then(function(){
			console.log("published to the server successfully");
			$scope.publishSuccessMessage = true;
		}, function(response){
			console.log("published to the server failed");
			$scope.publishFailureMessage = true;
			console.log(response.data);
		});
	}
	$scope.subscribe = function(){

		$scope.subscribeSuccessMessage = false;
		$scope.subscribeFailureMessage = false;
		console.log("Sending subscribe message to the server");
		console.log($scope.subscribeMessage);
		var queryParams = '?userid=' + $scope.userID + '&topic=' + $scope.subscribeMessage.topic;
		$scope.eventSource = new EventSource('/cs2372/rest/pubsub/subscribe' + queryParams);
		console.log("Subscribed to the topic successfully");
		
		$scope.eventSource.onmessage = function(event){
			console.log("Message received from the server");
			console.log(event.data);
			$scope.currentMessage = angular.fromJson(event.data);

			$scope.messages.push($scope.currentMessage);
			$scope.currentMessageIndex = $scope.messages.length - 1;
			console.log($scope.currentMessage.content);
			console.log($scope.currentMessage.topic);
			console.log($scope.currentMessage.location);
			
//			$scope.$apply(function(){
//				$scope.currentMessage = angular.fromJson(event.data);
//				$scope.currentMessageIndex = $scope.messages.length - 1;
//			});
			$scope.$apply();
		}
		$scope.subscribeSuccessMessage = true;
		$scope.subscribedTopics.push($scope.subscribeMessage.topic);
		
	}
	
	$scope.unSubscribe = function(){
		$scope.unSubscribeSuccessMessage = false;
		$scope.unSubscribeFailureMessage = false;
		$scope.unSubscribeMessage.userID = $scope.userID;
		console.log('Sending Unsubscribe request to the server');
		console.log($scope.unSubscribeMessage);
		$http.post('/cs2372/rest/pubsub/unsubscribe', $scope.unSubscribeMessage).then(function(){
			console.log("Unsubscribed to the topic successfully");
			$scope.unSubscribeSuccessMessage = true;
			var index = $scope.subscribedTopics.indexOf($scope.unSubscribeMessage.topic)
			$scope.subscribedTopics.splice(index, 1);	
		}, function(response){
			console.log("Unsubscribe to the topic failed");
			$scope.unSubscribeFailureMessage = true;
			console.log(response.data);
		});
	}
	
	$scope.getTopics = function(){
		var config = {};
		$http.get('/cs2372/rest/pubsub/topic', config).then(function(response){
			console.log("Get topics successful");
			console.log(response.data);
			$scope.topics = response.data;
		});
	}
	$scope.getUserID = function(){
		console.log("Getting userID from server");
		$http.get('/cs2372/rest/pubsub/userid').then(function(response){
			console.log("Get User ID successful");
			console.log("User ID: "+response.data);
			$scope.userID = response.data;
		})
	}
	$scope.createTopic = function(){
		$scope.createTopicSuccessMessage = false;
		$scope.createTopicFailureMessage = false;
		$scope.createTopicMessage.userID = $scope.userID;
		console.log("Sending CreateTopic message to the server");
		console.log($scope.createTopicMessage);
		$http.post('/cs2372/rest/pubsub/topic', $scope.createTopicMessage).then(function(){
			console.log("Created the topic successfully");
			$scope.createTopicSuccessMessage = true;
			$scope.topics.push($scope.createTopicMessage.topic);
		}, function(response){
			console.log("Create Topic failed");
			$scope.createTopicFailureMessage = true;
			console.log(response.data);
		});
	}
	
	$scope.getNextMessage = function(){
		if($scope.currentMessageIndex > 0){
			$scope.currentMessageIndex--;
			$scope.currentMessage = $scope.messages[$scope.currentMessageIndex];
		}
	}
	
	$scope.getPreviousMessage = function(){
		if($scope.currentMessageIndex < $scope.messages.length - 1){
			$scope.currentMessageIndex++;
			$scope.currentMessage = $scope.messages[$scope.currentMessageIndex];
		}
	}
	$scope.getTopics();
	$scope.getUserID();
	
	
//	var source = new EventSource('/collabedit/rest/pps/getupdate');
//	source.onmessage = function(event){
//		$scope.pps = event.data;
//		console.log("PPS Received from the server");
//		console.log(event.data);
//		$scope.$apply();
//	}
});